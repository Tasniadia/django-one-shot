from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/todo.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=post)
        context = {
            "post_object": post,
            "post_form": form,
        }
        return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
